<!DOCTYPE html>
<html lang="th">
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Restaurant Search By Google Map API</title>
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

</head>
<body>

<h1 class="mb-0 text-monospace  text-lg-center my-header text-center">SEARCH RESTAURANT</h1>


<div id="app" class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text text-success">&telrec;</span>
    </div>
    <input class="form-control form-control-lg"
           id="pac-input"
           type="text"
           placeholder="Type address or place ex. Bangsue">
    <span id="searchclear" onclick="clear_search()">&otimes;</span>
</div>

<div class="horizon-divide"></div>

<div id="map"></div>
<div id="infowindow-content">
    <img src="" width="16" height="16" id="place-icon">
    <span id="place-name" class="title"></span><br>
    <span id="place-address"></span>
</div>
<div id="right-panel">
    <h2 class="text-center badge-danger">RESTAURANT</h2>
    <ul id="places" class="list-group"></ul>
    <button id="more" class="btn btn-success">Load More</button>
</div>


</body>
</html>

<script src="{{ asset('js/app.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZ0EPA4w7Gz8NEFGIs9kPHSpuJG7A0N9M&libraries=places&callback=initMap&language=th&region=TH" async defer>

</script>

<script>

    var map;
    var marker;

    function initMap() {
        //Set Bangsue as default
        var lat = 13.806591;
        var long = 100.537794;

        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: lat, lng: long},
            zoom: 17
        });

        var geocoder = new google.maps.Geocoder;
        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');

        //Default map marked pin @ Bangsue
        marker_default(geocoder, map, infowindow, lat, long);

        // Init search restaurant NearBY Bangsue
        searchRestaurant(13.806591, 100.537794);


        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);


        autocomplete.addListener('place_changed', function () {

            var ListFound = document.getElementById('places');
            ListFound.innerHTML = "";
            var place = autocomplete.getPlace();
            var lat = place.geometry.location.lat();
            var long = place.geometry.location.lng()
            infowindow.setContent(infowindowContent);
            marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
            });


            infowindow.close();
            marker.setVisible(false);


            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);


            } // END else


            // Search restaurant NearBY Query
            searchRestaurant(lat, long);


            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);


        });


    }

    function clear_search() {
        document.getElementById('pac-input').value = '';
    }

    function marker_default(geocoder, map, infowindow, lat, long) {

        var latlng = {lat: lat, lng: long};
        geocoder.geocode({'location': latlng}, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    map.setZoom(17);
                    marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        anchorPoint: new google.maps.Point(0, -29)
                    });
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }

    function searchRestaurant(lat, long) {

        var set_location = {lat: lat, lng: long};


        // Create the places service.
        var service = new google.maps.places.PlacesService(map);
        var getNextPage = null;
        var moreButton = document.getElementById('more');
        moreButton.onclick = function () {
            moreButton.disabled = true;
            if (getNextPage) getNextPage();
        };

        var request = {
            location: set_location,
            radius: '500',  //meter
            type: ['restaurant']
        };

        // Perform a nearby search.
        service.textSearch(
            request,
            function (results, status, pagination) {
                if (status !== 'OK') return;
                createMarkers(results);
                moreButton.disabled = !pagination.hasNextPage;
                getNextPage = pagination.hasNextPage && function () {
                    pagination.nextPage();
                };
            });
    }

    function createMarkers(places) {
        var bounds = new google.maps.LatLngBounds();
        var placesList = document.getElementById('places');
        var rating;
        for (var i = 0, place; place = places[i]; i++) {
            var image = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            marker = new google.maps.Marker({
                map: map,
                icon: image,
                title: place.name,
                position: place.geometry.location
            });


            var li = document.createElement('li');
            li.setAttribute("class", "list-group-item d-flex justify-content-between align-items-center");
            var badge = '<span class="badge badge-warning badge-pill text-monospace">&#9733;' + place.rating + '</span>';
            li.innerHTML = '<a href="https://www.google.com/maps/place/?q=place_id:' + place.place_id + '" target="_blank">' + place.name + '</a>' + badge;
            placesList.appendChild(li);

            bounds.extend(place.geometry.location);
        }
        map.fitBounds(bounds);
    }

</script>