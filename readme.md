0. System Requirement PHP >= 7.1.3
1. git clone https://gridsada@bitbucket.org/gridsada/restaurantsearch.git
2. cd restaurantsearch && composer install
3. copy .env.example .env
4. php artisan key:generate
5. php artisan config:cache
6. php artisan serve
7. Enjoy search Restaurant By Place ( Default is Bangsue)

